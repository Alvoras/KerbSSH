#coding=Utf-8
from sys import argv, exit
import ConfigParser
import subprocess

config = ConfigParser.ConfigParser()
config.read('../config/kssh.cfg')

i = 0

for s in argv:
    argv[i] = s.lower()
    i = i + 1


def printOptions():
    print('Using the form :')
    print('section:')
    print('\tkey : value')
    print('\n')
    print('access :')
    print('\tport : [1-65535] (default 2200)')
    print('\n')
    print('display :')
    print('\tuse_unix_style : True/False (default False)')
    print('\n')
    print('authentication :')
    print('\tpermit_empty_password : True/False (default False)')
    print('\tpassword_authentication : True/False (default True)')
    print('\trsa_authentication : True/False (default True)')
    print('\trsa_key_directory : path/to/directory (default .ssh/)')
    print('\trsa_priv_key : filename (default \'id_rsa\')')
    print('\tauthorized_keys : filename (default \'authorized_keys\')')
    print('\tkerberos_authentication : True/False (default True)')
    print('\n')
    print('filter_access :')
    print('\tuse_classic_whitelist : True/False (default False)')
    print('\tclassic_whitelist_file : filename (default \'wl_classic\')')
    print('\tuse_classic_blacklist : True/False (default False)')
    print('\tclassic_blacklist_file : filename (default \'bl_classic\')')
    print('\tuse_kerberos_whitelist : True/False (default False)')
    print('\tkerberos_whitelist_file : filename (default \'wl_kerberos\')')
    print('\tuse_kerberos_blacklist : True/False (default False)')
    print('\tkerberos_blacklist_file : filename (default \'bl_kerberos\')')
    print('\tcascading_whitelist : kerberos/classic (default \'kerberos\')')
    print('\n')
    print('filter_commands :')
    print('\tuse_cmd_whitelist : True/False (default False)')
    print('\tcmd_whitelist_file : filename (default \'cmd_wl\')')
    print('\tuse_cmd_blacklist : True/False (default False)')
    print('\tcmd_blacklist_file : filename (default \'cmd_bl\')')
    print('\tallow_powershell : True/False (default False)')
    print('\tuse_ps_cmd_whitelist : True/False (default False)')
    print('\tps_cmd_whitelist_file : filename (default \'pscmd_wl\')')
    print('\tuse_ps_cmd_blacklist : True/False (default False)')
    print('\tps_cmd_blacklist_file : filename (default \'pscmd_bl\')')


if len(argv) == 1:
    print('Edit mode')
    print('\'exit\' ou \'quit\' : quitter le shell d\'édition')
    print('\'list\' ou \'show\' : afficher les différentes options disponibles')
    print('\'use <mode>\' : changer de mode.')
    print('Mode disponibles : edit, set (alias), get, fetch (alias)')
    print('\n')

    prefix = 'edit'
    loop = 1

    while loop:
        command = raw_input('(' + prefix + ')> ')
        cmdList = command.split(' ')
        cmdLen = len(cmdList)
        if cmdLen > 3:
            print('Trop d\'arguments (3 attendus, ' + str(cmdLen) + ' donné(s))')
        elif cmdLen < 3:
            if cmdList[0] == 'use':
                if cmdList[1] in {'get', 'fetch'}:
                    prefix = 'get'
                    print('Mode get activé')
                elif cmdList[1] in {'edit', 'set'}:
                    prefix = 'edit'
                    print('Mode edit activé')
                else:
                    print('Mode inconnu')
            elif prefix == 'get' and cmdLen == 2:
                print(config.get(cmdList[0], cmdList[1]))
            elif cmdList[0] in {'quit', 'exit'}:
                loop = 0
            elif cmdList[0] in {'list', 'show'}:
                printOptions()
            else:
                print('Pas assez d\'arguments (3 attendus, ' + str(cmdLen) + ' donné(s))')
        else:
            try:
                if prefix == 'edit':
                    config.set(cmdList[0], cmdList[1], cmdList[2])
                    try:
                        with open('../config/kssh.cfg', 'wb') as configfile:
                            config.write(configfile)
                    except IOError as e:
                        print('Failed to create the file.\n' + e)
                        exit(0)

                print('Modification effectuée')
            except:
                print('Commande invalide')
    exit(1)
if len(argv) >= 1 and argv[1] == 'reset':
    subprocess.call('start python ./scripts/kssh_init_conf.py')
    exit(1)
elif argv[1] == 'list':
    printOptions()
elif argv[1] == 'edit':
    if len(argv) != 5:
        argvLength = len(argv)
        print('Not enough argument (3 options needed, ' + str(argvLength-2) + ' provided). Use \'help\' to get the avalaible options.')
        exit(1)
    try:
        config.set(argv[2], argv[3], argv[4])
        if argv[4] == 'true':
            argv[4] = 'True'
        try:
            with open('../config/kssh.cfg', 'wb') as configfile:
               config.write(configfile)
        except IOError as e:
            print('Failed to create the file.\n' + e)
            exit(0)
    except Exception as e:
        print('Failed to edit the file.\n' + e)
        exit(1)
elif argv[1] == 'help':
    print('Options :')
    print('\tReset : reset the kssh.cfg file inside the .config directory')
    print('\n')
    print('\tEdit : Edit the config file. You need to specify the section, the key and the value you want to update to')
    print('\t\tExample : start python kssh_init_editor.py edit access port 2222')
    print('\t> This command will update the \'port\' key to the value \'2222\' within the \'access\' section')
    print('\n')
    print('\tList : List all the available options in the configuration file with their default values')
else:
    print('Invalid option. Use \'help\' to get the avalaible options')

