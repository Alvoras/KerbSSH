# coding: utf-8
import ConfigParser

config = ConfigParser.RawConfigParser(allow_no_value=True)

# Keep case sensitive
config.optionxform = str

config.add_section('welcome')
config.set('welcome', '# KerbSSHv1.0')
config.set('welcome', '# This is the kssh server config file.')
config.set('welcome', '# Commented lines starts with \'#\'.')
config.set('welcome', '# This config file is created by the kssh_init_conf.py script.')
config.set('welcome', '# Please DO NOT TRY to edit this file manually. Instead please use the update_conf.py script provided.')

config.add_section('access')
config.set('access', '# The port on which the server is listening')
config.set('access', '# Please note that if you want to use a port lesser than 1000, you need to start the server with admin privileges')
config.set('access', 'Port', 2200)

config.add_section('display')
config.set('access', '# Replace the home directory path with "~"')
config.set('access', 'use_unix_style', False)

config.add_section('authentication')
config.set('authentication', '# Domain policy can override this, so please contact your domain admin first to see if this option is compatible with your domain\'s policy')
config.set('authentication', 'permit_empty_password', False)
config.set('authentication', 'password_authentication', True)
config.set('authentication', '\n# Use private key / public key authentication')
config.set('authentication', '# NOTE : The keys must follow the OpenSSH standard')
config.set('authentication', 'rsa_authentication', False)
config.set('authentication', '\n# The path to the key directory')
config.set('authentication', 'rsa_key_directory', '.ssh/')
config.set('authentication', '\n# The private key filename')
config.set('authentication', 'rsa_priv_key', 'id_rsa')
config.set('authentication', '\n# The path to the file containing all the authorized keys')
config.set('authentication', 'authorized_keys', 'authorized_keys')
config.set('authentication', '\n# Use kerberos authentication')
config.set('authentication', 'kerberos_authentication', True)

config.add_section('filter_access')
config.set('filter_access', '# NOTE : the blacklist overrides the whitelist')
config.set('filter_access', '\n# Filter users using a whitelist')
config.set('filter_access', 'use_classic_whitelist', False)
config.set('filter_access', 'classic_whitelist_file', 'wl_classic')
config.set('filter_access', '\n# Filter users using a blacklist')
config.set('filter_access', 'use_classic_blacklist', False)
config.set('filter_access', 'classic_blacklist_file', 'bl_classic')

config.set('filter_access', '\n# Filter kerberos users using a whitelist')
config.set('filter_access', 'use_kerberos_whitelist', False)
config.set('filter_access', 'kerberos_whitelist_file', 'wl_kerberos')
config.set('filter_access', '\n# Filter kerberos users using a blacklist')
config.set('filter_access', 'use_kerberos_blacklist', False)
config.set('filter_access', 'kerberos_blacklist_file', 'bl_kerberos')

config.set('filter_access', '\n# The specified list is also applied to the other file')
config.set('filter_access', 'use_cascading', True)

config.set('filter_access', '\n# By default, the kerberos whitelist and blacklist are also applied on the other form of authentication')
config.set('filter_access', '# You can choose to override \'kerberos\' with \'classic\' to reverse this (the classic list will then be applied to the kerberos list)')
config.set('filter_access', 'cascading_whitelist', 'kerberos')
config.set('filter_access', 'cascading_blacklist', 'kerberos')

config.add_section('filter_commands')
config.set('filter_commands', '# You can filter the commands that can be executed from the clients based on the content of a whitelist or a blacklist')
config.set('filter_commands', '\n## Classic commands\n')
config.set('filter_commands', 'use_cmd_whitelist', False)
config.set('filter_commands', 'cmd_whitelist_file', 'cmd_wl')

config.set('filter_commands', 'use_cmd_blacklist', False)
config.set('filter_commands', 'cmd_blacklist_file', 'cmd_bl')

config.set('filter_commands', '\n## Powershell commands\n')
config.set('filter_commands', '# Allow your clients to execute powershell commands')
config.set('filter_commands', 'allow_powershell', False)
config.set('filter_commands', 'use_ps_cmd_whitelist', False)
config.set('filter_commands', 'ps_cmd_whitelist_file', 'pscmd_wl')

config.set('filter_commands', 'use_ps_cmd_blacklist', False)
config.set('filter_commands', 'ps_cmd_blacklist_file', 'pscmd_bl')

with open('../config/kssh.cfg', 'wb') as configfile:
    config.write(configfile)
