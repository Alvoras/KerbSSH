# coding=utf-8

from binascii import hexlify
import socket
import sys
import threading
import traceback
import paramiko
from paramiko.py3compat import b, u, decodebytes

import logging

from subprocess import *
from os import environ
from os import path

import ConfigParser

import platform

if platform.system().lower() == 'windows':
    from ctypes import windll, byref, POINTER, FormatError, GetLastError
    from ctypes.wintypes import (LPCWSTR, WCHAR, DWORD, HANDLE, BOOL)
    # Window's Logon API
    # GLOBALS
    MAX_COMPUTERNAME_LENGTH = 15
    LOGON32_LOGON_NETWORK_CLEARTEXT = 8
    LOGON32_PROVIDER_DEFAULT = 0

    # SETUP
    CloseHandle = windll.Kernel32.CloseHandle
    CloseHandle.argtypes = [HANDLE]
    CloseHandle.restype = BOOL

    LogonUser = windll.Advapi32.LogonUserW
    LogonUser.argtypes = [LPCWSTR, LPCWSTR, LPCWSTR, DWORD, DWORD, POINTER(HANDLE)]
    LogonUser.restype = BOOL
elif platform.system().lower() == 'linux':
    import pam
elif platform.system().lower() == 'posix':
    print('[FAIL] OS not supported')
    sys.exit(1)

# CONFIG
config = ConfigParser.ConfigParser()
config.read('./config/kssh.cfg')

# CONSTANT
CRLF = '\r\n'

# GLOBALS
DoGSSAPIKeyExchange = True

# Start logging
logging.basicConfig()
logger = logging.getLogger()

# Fetch host key
rsaKeyDirectory = config.get('authentication', 'rsa_key_directory')
rsaPrivateKey = config.get('authentication', 'rsa_priv_key')
rsaAuthorizedKeysPath = rsaKeyDirectory + config.get('authentication', 'authorized_keys')

host_key = paramiko.RSAKey(filename=rsaKeyDirectory + rsaPrivateKey)
print('[INFO] Read key: ' + u(hexlify(host_key.get_fingerprint())))


# Tries to connect to local Window's account with given credentials
def tryLogon(username, password):
    currentPlatform = platform.system().lower()

    if currentPlatform == 'windows':
        token = HANDLE()
        status = LogonUser(username,
                           environ['COMPUTERNAME'],
                           password,
                           LOGON32_LOGON_NETWORK_CLEARTEXT,
                           LOGON32_PROVIDER_DEFAULT,
                           byref(token))
        error = GetLastError()

        if status:
            CloseHandle(token)
            return True
        else:
            print('[FAIL] ' + FormatError(error))
            return False
    elif currentPlatform == 'linux':
        return pam.authenticate(username, password, service='login')


# Returns the environment variable from the host machine to display the shell info accordingly
# @info : the string to be echoed that retrieve the variable, i.e "%USERNAME%
def fetchEnvInfo(info):
    if platform.system().lower() == 'posix':
        print('[FAIL] MacOS shell is not supported yet')
        ret = 'UNSUPPORTED'
    else:
        out = Popen('echo '+info, shell=True, stdout=PIPE)
        ret = out.communicate()
        if info == '$HOSTNAME':
            out = Popen('cat /etc/hosts|grep 127.0.1.1|tr "\t" "\n"|tail -n1', shell=True, stdout=PIPE)
            ret = out.communicate()
        elif ret[0].strip() == info:
            return False

    return ret[0].strip(CRLF)


def loadList(path):
    return open(path).read().splitlines()


def isUserWhitelisted(username, wltype):
    wlFile = config.get('filter_access', wltype + '_whitelist_file')
    wl = loadList('./config/whitelists/' + wlFile)
    cascadingList = config.get('filter_access', 'cascading_blacklist')

    if username in wl:
        return True
    # If we're using cascading, then try the other list
    elif config.getboolean('filter_access', 'use_cascading') and wlFile.split('_')[1] != cascadingList:
        otherList = 'kerberos_whitelist_file' if cascadingList == 'classic' else 'classic_whitelist_file'
        wl = loadList('./config/whitelists/' + config.get('filter_access', otherList))
        if username in wl:
            return True

    return False


def isUserBlacklisted(username, bltype):
    blFile = config.get('filter_access', bltype + '_blacklist_file')
    bl = loadList('./config/blacklists/' + blFile)
    cascadingList = config.get('filter_access', 'cascading_blacklist')

    if username in bl:
        return True
    # If we're using cascading, then try the other list
    elif config.getboolean('filter_access', 'use_cascading') and blFile.split('_')[1] != cascadingList:
        otherList = 'kerberos_blacklist_file' if cascadingList == 'classic' else 'classic_blacklist_file'
        bl = loadList('./config/blacklists/' + config.get('filter_access', otherList))
        if username in bl:
            return True

    return False


def isCommandWhitelisted(cmd, wltype):
    splitCmd = cmd.split(' ')

    checkCmd = splitCmd[1] if splitCmd[0] in {'Powershell', 'powershell'} else splitCmd[0]

    cmdFile = config.get('filter_commands', wltype + '_whitelist_file')
    wl = loadList('./config/whitelists/' + cmdFile)

    if checkCmd in wl:
        return True

    return False


def isCommandBlacklisted(cmd, bltype):
    splitCmd = cmd.split(' ')

    checkCmd = splitCmd[1] if splitCmd[0] in {'Powershell', 'powershell'} else splitCmd[0]

    cmdFile = config.get('filter_commands', bltype + '_blacklist_file')
    bl = loadList('./config/blacklists/' + cmdFile)

    if checkCmd in bl:
        return True

    return False


def handleCmd(chan, usePowershell, command):
    filterCommand = False
    if command in {'use powershell', 'use ps'}:
        if config.getboolean('filter_commands', 'allow_powershell'):
            command = 'powershell'
            usePowershell = True
        else:
            chan.send(CRLF + 'L\'utilisation de powershell a été désactivé sur cette machine.')
            filterCommand = True
    elif 'powershell' in command:
        if not config.getboolean('filter_commands', 'allow_powershell'):
            chan.send(CRLF + 'L\'utilisation de powershell a été désactivé sur cette machine.')
            filterCommand = True
    elif command in {'exit powershell', 'exit ps'}:
        if usePowershell:
            usePowershell = False
            filterCommand = True
            chan.send(CRLF + 'Powershell a bien été quitté')
    elif command in {'exit', 'quit'}:
        print('[INFO] User requested exit')
        chan.send(CRLF)
        chan.close()

    # Recheck allow_powershell config to prevent any program's logic fail to be exploited
    if usePowershell and config.getboolean('filter_commands', 'allow_powershell'):
        if config.getboolean('filter_commands', 'use_ps_cmd_blacklist'):
            if isCommandBlacklisted(command, 'ps_cmd'):
                chan.send(CRLF + 'Cette commande a été désactivé')
                filterCommand = True
        elif config.getboolean('filter_commands', 'use_ps_cmd_whitelist'):
            if not isCommandWhitelisted(command, 'ps_cmd'):
                chan.send(CRLF + 'Cette commande a été désactivé')
                filterCommand = True
        else:
            command = "powershell " + command

    # Check if command is filtered
    if config.getboolean('filter_commands', 'use_cmd_blacklist'):
        if isCommandBlacklisted(command, 'cmd'):
            chan.send(CRLF + 'Cette commande a été désactivé')
            filterCommand = True
    elif config.getboolean('filter_commands', 'use_cmd_whitelist'):
        if not isCommandWhitelisted(command, 'cmd'):
            chan.send(CRLF + 'Cette commande a été désactivé')
            filterCommand = True

    if not filterCommand:
        print("[INFO] Running " + command)

        out = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = out.communicate()
        if stdout != '':
            chan.send(CRLF + stdout.replace('\n', CRLF)[:-2])
        if stderr != '':
            chan.send(CRLF + stderr.replace('\n', CRLF)[:-2])


def closeConnection(chan, t):
    chan.close()
    t.close()


def loadAuthorizedKeys():
    availablePubKeys = []
    with open(rsaAuthorizedKeysPath) as authKeyFile:
        for line in authKeyFile:
            keyParts = line.split(" ")
            if keyParts[0] != 'ssh-rsa':
                continue

            availablePubKeys.append(paramiko.RSAKey(data=decodebytes(keyParts[1])))

    return availablePubKeys


class Server(paramiko.ServerInterface):
    availablePublicKeys = loadAuthorizedKeys()

    def __init__(self):
        self.event = threading.Event()

    def check_channel_request(self, kind, chanid):
        if kind == 'session':
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_password(self, username, password):
        if config.getboolean('authentication', 'password_authentication'):
            # Check if we're using a blacklist
            if config.getboolean('filter_access', 'use_classic_blacklist'):
                if isUserBlacklisted(username, 'classic'):
                    return paramiko.AUTH_FAILED
            elif config.getboolean('filter_access', 'use_classic_whitelist'):
                # Then, check if we're using a blacklist
                if not isUserWhitelisted(username, 'classic'):
                    return paramiko.AUTH_FAILED
            else:
                if password == '' and not config.getboolean('authentication', 'permit_empty_password'):
                    return paramiko.AUTH_FAILED

                #  Checking the username would only allow the client to connect as client running the ssh server
                #  Instead we want to connect as any user in the ssh's server database, so no username == USERNAME check
                if tryLogon(USERNAME, password):
                    return paramiko.AUTH_SUCCESSFUL
                else:
                    print('[FAIL] User not found')
                    # We don't return auth failed as the ssh program will handle rejection by itself
        return paramiko.AUTH_FAILED

    def check_auth_publickey(self, username, key):
        if config.getboolean('authentication', 'rsa_authentication'):
            print('[INFO] Auth attempt with key: ' + u(hexlify(key.get_fingerprint())))

            # Check if we're using a whitelist
            if config.getboolean('filter_access', 'use_classic_blacklist'):
                if isUserBlacklisted(username, 'classic'):
                    return paramiko.AUTH_FAILED
            # Then, check if we're using a blacklist
            elif config.getboolean('filter_access', 'use_classic_whitelist'):
                if not isUserWhitelisted(username, 'classic'):
                    return paramiko.AUTH_FAILED
            else:
                if key in self.availablePublicKeys:
                    return paramiko.AUTH_SUCCESSFUL

                return paramiko.AUTH_FAILED


    def check_auth_gssapi_with_mic(self, username, gss_authenticated=paramiko.AUTH_FAILED, cc_file=None):
        if config.getboolean('authentication', 'kerberos_authentication'):
            # Check if we're using a whitelist
            if config.getboolean('filter_access', 'use_kerberos_blacklist'):
                if isUserBlacklisted(username, 'kerberos'):
                    return paramiko.AUTH_FAILED
            # Then, check if we're using a blacklist
            elif config.getboolean('filter_access', 'use_kerberos_whitelist'):
                if not isUserWhitelisted(username, 'kerberos'):
                    return paramiko.AUTH_FAILED
            else:
                if gss_authenticated == paramiko.AUTH_SUCCESSFUL:
                    return paramiko.AUTH_SUCCESSFUL

        return paramiko.AUTH_FAILED

    def check_auth_gssapi_keyex(self, username, gss_authenticated=paramiko.AUTH_FAILED, cc_file=None):
        if config.getboolean('authentication', 'kerberos_authentication'):
            # Check if we're using a whitelist
            if config.getboolean('filter_access', 'use_kerberos_blacklist'):
                if isUserBlacklisted(username, 'kerberos'):
                    return paramiko.AUTH_FAILED
            # Then, check if we're using a blacklist
            elif config.getboolean('filter_access', 'use_kerberos_whitelist'):
                if not isUserWhitelisted(username, 'kerberos'):
                    return paramiko.AUTH_FAILED
            else:
                if gss_authenticated == paramiko.AUTH_SUCCESSFUL:
                    return paramiko.AUTH_SUCCESSFUL

        return paramiko.AUTH_FAILED

    def enable_auth_gssapi(self):
        if config.getboolean('authentication', 'kerberos_authentication'):
            return True
        else:
            return False

    def get_allowed_auths(self, username):
        allowed_list = []

        if config.getboolean('authentication', 'kerberos_authentication'):
            allowed_list.append('gssapi-keyex')
            allowed_list.append('gssapi-with-mic')
        if config.getboolean('authentication', 'rsa_authentication'):
            allowed_list.append('publickey')
        if config.getboolean('authentication', 'password_authentication'):
            allowed_list.append('password')

        return ', '.join(allowed_list)

    def check_channel_shell_request(self, channel):
        self.event.set()
        return True

    def check_channel_pty_request(self, channel, term, width, height, pixelwidth,
                                  pixelheight, modes):
        return True


# Dedicated thread for each clients
class ClientHandler(threading.Thread):
    def __init__(self, client):
        threading.Thread.__init__(self)
        self.client = client
        self.username = USERNAME

    def run(self):
        print('[INFO] New client connected')
        try:
            t = paramiko.Transport(self.client, gss_kex=DoGSSAPIKeyExchange)
            t.set_gss_host(socket.getfqdn(""))
            try:
                t.load_server_moduli()
            except:
                print('([FAIL] Failed to load moduli -- gex will be unsupported.)')
                raise
            t.add_server_key(host_key)
            server = Server()
            try:
                t.start_server(server=server)
            except paramiko.SSHException:
                print('[FAIL] SSH negotiation failed.')
                t.close

            # wait for auth
            chan = t.accept(20)
            if chan is None:
                print('[FAIL] No channel.')
                t.close
                exit(0)
            print('[OK] Client authenticated')

            chan.send('[OK]Authenticated using public key.' + CRLF)

            server.event.wait(10)
            if not server.event.is_set():
                print('[FAIL] Client never asked for a shell.')
                sys.exit(1)

            f = chan.makefile('rU')

            usePowershell = False
            workingDir = fetchEnvInfo('%CD%') or fetchEnvInfo('$PWD')
            if config.get('display', 'use_unix_style'):
                workingDir.replace(path.expanduser('~'), '~')

            shellSeparator = '::' if platform.system().lower() == 'windows' else ':'
            shellBase = self.username + '@' + HOSTNAME + shellSeparator + workingDir + '$ '

            command = ''

            while True:
                buf = shellBase
                shellBase = CRLF + self.username + '@' + HOSTNAME + shellSeparator + workingDir + '$ '
                if usePowershell:
                    shellBase = "PS " + shellBase
                while True:
                    chan.send(buf)
                    buf = f.read(1)
                    command += buf
                    if buf == '\x7f':  # Backspace
                        padding = ' '
                        command = command[0:-2]
                        #  \033[F ANSI escape sequence
                        buf = '\033[F' + shellBase + str(command + padding) + '\033[F' + shellBase + str(command)
                    elif buf == '\r':  # Hit enter
                        break

                handleCmd(chan, usePowershell, command.strip())
                command = ''

        except Exception as e:
            print('[FAIL] Caught exception: ' + str(e.__class__) + ': ' + str(e))
            traceback.print_exc()
            try:
                t.close()
            except:
                pass
            sys.exit(1)


def listen():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('', config.getint('access', 'port')))
    except Exception as e:
        print('[FAIL] Bind failed: ' + str(e))
        traceback.print_exc()
        sys.exit(1)

    try:
        sock.listen(100)
        print('[INFO] Listening for connection ...')
        client, addr = sock.accept()
    except Exception as e:
        print('[FAIL] Listen/accept failed: ' + str(e))
        traceback.print_exc()
        sys.exit(1)

    print('[INFO] Got a connection')

    newClient = ClientHandler(client)

    newClient.start()


USERNAME = fetchEnvInfo('%USERNAME%') or fetchEnvInfo('$LOGNAME')
HOSTNAME = fetchEnvInfo('%COMPUTERNAME%') or fetchEnvInfo('$HOSTNAME')

while True:
    try:
        listen()
    except KeyboardInterrupt:
        try:
            if chan is not None:
                chan.close()
            if t is not None:
                t.close()
        except Exception as e:
            print(e)
        sys.exit(0)
