# KerbSSHv1.0

## Description
KerbSSH est un serveur SSH compatible avec l'autentification par mot de passe, clef RSA et ticket kerberos (à partir du cache du client).

## IMPORTANT
#### Accès
- Le port choisi doit être ouvert (par défaut : 2200) et accessible (si le trafic est routé jusqu'à la machine, le port doit être accessible à travers le routeur)
- La machine doit être connectée à Internet lors de l'installation

#### Compatibilité Kerberos :
- L’hôte et le client doivent faire parti d’un même domaine Active Directory et le client doit s'être authentifié auprès d’un serveur Kerberos à l’ouverture de la session. La machine sur laquelle tourne le serveur doit pouvoir communiquer avec le KDC.

#### Compatibilité Paramiko

- Le paquet python « gssapi » ne doit pas être installé.
